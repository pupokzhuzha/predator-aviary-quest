package com.Predator.Aviary.Quest.birds.app.menu

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.Predator.Aviary.Quest.birds.app.R
import com.Predator.Aviary.Quest.birds.app.databinding.FragmentMainMenuBinding

class MainMenuFragment : Fragment() {

    private var binding: FragmentMainMenuBinding? = null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainMenuBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            birds.setOnClickListener {
                findNavController().navigate(R.id.action_mainMenuFragment_to_birdFragment)
            }
            exit.setOnClickListener {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}