package com.Predator.Aviary.Quest.birds.app.bird

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.Predator.Aviary.Quest.birds.app.BirdInfo
import com.Predator.Aviary.Quest.birds.app.ObjectGen
import com.Predator.Aviary.Quest.birds.app.R
import com.Predator.Aviary.Quest.birds.app.databinding.FragmentBirdBinding
import com.Predator.Aviary.Quest.birds.app.databinding.FragmentMainMenuBinding

class BirdFragment : Fragment() {

    private var binding: FragmentBirdBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private lateinit var bird : BirdInfo

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBirdBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bird = ObjectGen.gen(page)
        mBinding.apply {
            tittle.text = bird.tittle
            desc.text = bird.desc
            image.setImageResource(bird.image)

            next.setOnClickListener {
                page++
                bird = ObjectGen.gen(page)
                tittle.text = bird.tittle
                desc.text = bird.desc
                image.setImageResource(bird.image)

                if(page == 14){
                    next.isEnabled = false
                    shadow.visibility = View.VISIBLE
                }
            }
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}