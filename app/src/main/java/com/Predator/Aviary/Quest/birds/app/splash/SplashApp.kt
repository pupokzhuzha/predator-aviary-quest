package com.Predator.Aviary.Quest.birds.app.splash

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.Predator.Aviary.Quest.birds.app.MainActivity
import com.Predator.Aviary.Quest.birds.app.R
import com.Predator.Aviary.Quest.birds.app.help.replaceActivity

@SuppressLint("CustomSplashScreen")
class SplashApp : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_app)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(MainActivity())
        }, 500)
    }
}